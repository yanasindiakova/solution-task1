package spotify.tests;

import spotify.entities.UserCreation;
import org.testng.annotations.Test;
import spotify.pages.LoginPage;

import static org.testng.Assert.assertEquals;

public class LoginTest extends BaseTest {
    private static final String VALID_USER_NAME = "validUser";
    private static final String INVALID_USER_NAME = "invalidUser";
    private static final String EXPECTED_EMPTY_USERNAME_ERROR = "Please enter your Spotify username or email address.";
    private static final String EXPECTED_EMPTY_PASSWORD_ERROR = "Please enter your password.";
    private static final String EXPECTED_INVALID_CREDENTIALS_ERROR = "Incorrect username or password.";


    @Test(description = "Login with empty credentials",
            priority = 10)
    public void loginWithEmptyCredentials() {
        LoginPage loginPage = getMainPage()
                .clickLoginButton()
                .fillLoginForm(VALID_USER_NAME)
                .clearUserNameInputCustom()
                .clearPasswordInputCustom();
        softAssert.assertEquals(EXPECTED_EMPTY_USERNAME_ERROR, loginPage.getEmptyUserNameMessageText());
        softAssert.assertEquals(EXPECTED_EMPTY_PASSWORD_ERROR, loginPage.getEmptyPasswordMessageText());
        softAssert.assertAll();

    }

    @Test(description = "Login with invalid credentials",
            priority = 20)
    public void loginWithInvalidCredentials() {
        getMainPage()
                .clickLoginButton()
                .fillLoginForm(INVALID_USER_NAME)
                .clickLoginButton();
        String actualInvalidCredentialsErrorMessage = getLoginPage()
                .getInvalidCredentialsMessageText();
        assertEquals(actualInvalidCredentialsErrorMessage, EXPECTED_INVALID_CREDENTIALS_ERROR);

    }

    @Test(description = "Login with valid credentials",
            priority = 30)
    public void loginWithValidCredentials() {
        getMainPage()
                .clickLoginButton()
                .fillLoginForm(VALID_USER_NAME)
                .clickLoginButton();

        String actualLoggedUserName = getMainPage().getLoggedInUserName();
        String expectedLoggedUserName = new UserCreation(VALID_USER_NAME).getUser().getUserName();

        assertEquals(actualLoggedUserName, expectedLoggedUserName);

    }

}
