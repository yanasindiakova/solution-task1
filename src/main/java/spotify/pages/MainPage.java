package spotify.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BasePage {
    @FindBy(xpath = "//button[@data-testid='login-button']")
    private WebElement loginButton;

    @FindBy(xpath = "//button[@data-testid = 'user-widget-link']")
    private WebElement userIcon;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage clickLoginButton() {
        clickElement(loginButton);
        return new LoginPage(driver);
    }

    public String getLoggedInUserName() {
        waitForPageLoadComplete();
        waitVisibilityOfElement(userIcon);
        return userIcon.getAttribute("aria-label");
    }
}
