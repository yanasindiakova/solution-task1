package spotify.pages;

import spotify.entities.User;
import spotify.entities.UserCreation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
    private User user;

    @FindBy(id = "login-username")
    private WebElement userNameInput;

    @FindBy(id = "login-password")
    private WebElement passwordInput;

    @FindBy(id = "login-button")
    private WebElement loginButton;

    @FindBy(xpath = "//p[@class= 'Type__TypeElement-sc-goli3j-0 gkqrGP sc-bqWxrE iJlXnD']")
    private WebElement emptyUserNameMessage;

    @FindBy(xpath = "//div[@data-testid='password-error']//span")
    private WebElement emptyPasswordMessage;

    @FindBy(xpath = "//span[@class='Message-sc-15vkh7g-0 dHbxKh']")
    private WebElement invalidCredentialsError;


    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void setUser(String name) {
        this.user = new UserCreation(name).getUser();
    }

    public LoginPage inputUserName(String name) {
        waitVisibilityOfElement(userNameInput);
        userNameInput.sendKeys(user.getUserName());
        return this;
    }

    public LoginPage inputPassword(String name) {
        waitVisibilityOfElement(passwordInput);
        passwordInput.sendKeys(user.getPassword());
        return this;
    }

    public LoginPage clearUserNameInput() {
        userNameInput.clear();
        return this;
    }

    public LoginPage clearUserNameInputCustom() {
        clearInput(userNameInput, user.getUserName());
        return this;
    }

    public LoginPage clearPasswordInputCustom() {
        clearInput(passwordInput, user.getPassword());
        return this;
    }


    public LoginPage clearPasswordInput() {
        passwordInput.clear();
        return this;
    }

    public LoginPage fillLoginForm(String name) {
        setUser(name);
        waitForPageLoadComplete();
        inputUserName(name)
                .inputPassword(name);
        return this;
    }

    public void clickLoginButton() {
        clickElement(loginButton);
    }

    public String getEmptyUserNameMessageText() {
        waitVisibilityOfElement(emptyUserNameMessage);
        return emptyUserNameMessage.getText();
    }

    public String getEmptyPasswordMessageText() {
        waitVisibilityOfElement(emptyPasswordMessage);
        return emptyPasswordMessage.getText();
    }

    public String getInvalidCredentialsMessageText() {
        waitVisibilityOfElement(invalidCredentialsError);
        return invalidCredentialsError.getText();
    }
}
